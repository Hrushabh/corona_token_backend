module.exports = mongoose => {
    var schema =  mongoose.Schema(
        {
          _id: Number, 
          name: String,
          weeklyTokens: Number,
          dailyTokens: Number,
          currentTokens: Number,
          url1: String,
          url2: String,
          url3: String,
          url4: String,
          url5: String,
          description: String
        }
      );
    const places = mongoose.model('Places', schema);
    return places;

  };