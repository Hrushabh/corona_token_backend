module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          email: {type: String, unique: true},
          order: [{_id: Number, place: String, tickets: Number, Date: Date}]
        }
      );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const order = mongoose.model("Order", schema)
    return order;
  };