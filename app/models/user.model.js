module.exports = mongoose => {
  var schema = mongoose.Schema(
      {
        email:  {type: String, unique: true},
        password: String,
        name: String,
        address: String,
        mobile: Number
      },
      { timestamps: true }
    );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const user = mongoose.model("User", schema)
  return user;
};