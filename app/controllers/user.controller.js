const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { user } = require("../models");

////////////// To save the data of user in db after signup ////////////////
exports.signUp = (req, res) => {
  // Validate request
  if (!req.body.email) {
    res.status(400).send({ message: "Email can not be empty!" });
    return;
  }
  // Create a user
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    name: req.body.name,
    address: req.body.address,
    mobile: parseInt(req.body.mobile)
  });

  // Save user in the database
  user
    .save(user)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the user."
      });
    });
};

////////////// Validate User Id and Password for Sign Up ////////////////
exports.loginCheck = (req,res) => 
{
  User.findOne({
    email: req.body.email
  })
  .exec((err, user) =>
    {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });
      console.log
      res.status(200).send({
        id: user._id,
        name: user.name,
        email: user.email,
        accessToken: token
      });
    });
};

  // Validate request
  // if (!req.body.email) 
  // {
  //     res.status(400).send({ message: "Email can not be empty!" });
  //     return;
  // }

  // Store email and password
  // const  email= req.body.email;
  // const password= req.body.password;
//   User
//   .find({email:email},function(err,docs)
//   {
//       if(docs[0].password==password)
//       {
//           res.send(true);
//       }
//       else
//       {
//           res.send(false);
//       }
//   })
//   .catch(err => {
//     res.status(500).send({
//       message:
//         err.message || "Some error occurred while validating the User."
//     });
//   });
// };