const db = require("../models");
const Places = db.places;

// exports.findAll = (req, res) => {
//     const url1 = req.query.url1;
//     var condition = url1 ? { url1: { $regex: new RegExp(url1), $options: "i" } } : {};
  
//     Places.find(condition)
//       .then(data => {
//         res.send(data);
//       })
//       .catch(err => {
//         res.status(500).send({
//           message:
//             err.message || "Some error occurred while retrieving tutorials."
//         });
//       });
// };

exports.updateToken = (req, res,) => {
  Places.findOneAndUpdate(
      {_id: req.body.id},
      {currentTokens: req.body.currentTokens}, { useFindAndModify: false }, function (err, place) {
        if (err) {
          res.status(400).send(err);
         } 
         else if (!place) {
          res.status(404).send("Place with particular ID not found.");
         } else {
          res.status(200).send(place);
         }
  });
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Places.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Place with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Place with id=" + id });
    });
};