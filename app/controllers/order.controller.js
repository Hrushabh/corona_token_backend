const db = require("../models");
const Order = db.order;

////////////// To add a initial record when the user registers ////////////////
exports.createInitialRecord = (req, res) => {
  // Validate request
  if (!req.body.email) {
    res.status(400).send({ message: "Email can not be empty!" });
    return;
  }
  // Create a record
  const order = new Order({
    email: req.body.email,
    order: []
  });

  // Save record in the database
  order
    .save(order)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the record."
      });
    });
};

////////////// To update the orderDetail of the user when they buy tickets ////////////////
exports.updateOrderDetails = (req, res,) => {
    var orderTemp = {"_id":1, "place": req.body.place, "tickets": req.body.tickets, "Date": new Date()};
    Order.findOneAndUpdate(
        {email: req.body.email},
        {$push: {order: orderTemp}}, { useFindAndModify: false }, function (err, data) {
            if (err) {
                console.log(err);
            } 
            else if(!data)
            {
              res.status(404).send("Email Not Found");
            }
            else {
                res.send(data);
            }
    });
};


////////////// To show order details of a particular email ////////////////
exports.getAllOrders = (req, res) => {
  if (!req.body.email) {
    res.status(400).send({ message: "Email can not be empty!" });
    return;
  }

    const email=req.body.email;

    Order.findOne({'email': email}, function(err, order){
      if (err) {
       res.status(400).send(err);
      } else if (!order) {
       res.status(404).send("Email Not Found");
      } else {
       res.status(200).send(order.order);
    }
  });
};