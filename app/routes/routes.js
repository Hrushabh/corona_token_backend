module.exports = app => {
  const user = require("../controllers/user.controller.js");
  const places = require("../controllers/places.controller.js");
  const order = require("../controllers/order.controller.js");
  const {verifySignUp} = require("../middlewares");
  var router = require("express").Router();

  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  ////////////// To save the data of user in db after signup ////////////////
  router.post("/register",
              [verifySignUp.checkDuplicateUsernameOrEmail
              ],
              user.signUp);

  ////////////// To check the login details of the user ////////////////
  router.post("/login", user.loginCheck);

  ////////////// To add a initial record when the user registers ////////////////
  router.post("/add", order.createInitialRecord);

  ////////////// To update the orderDetail of the user when they buy tickets  ////////////////
  router.put("/buy", order.updateOrderDetails);

  ////////////// To show order details of a particular email ////////////////
  router.get("/orderDetails", order.getAllOrders);

  ////////////// To update the tokens of the place when they buy tickets in placeDetails table ////////////////
  router.put("/change", places.updateToken);

  router.get("/places/:id", places.findOne);

  app.use("/api", router);
};